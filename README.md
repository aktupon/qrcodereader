# Installation
Install yarn with: `npm i -g yarn`.

Afterwards, run `yarn` to install dependencies.

# Tests
run `yarn test` to run unit tests.

run `yarn flow` to run type checks.

# Run on devices
## For IOS
run `react-native run-ios`

## For Android
run `react-native run-android`
