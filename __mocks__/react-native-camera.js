import React, { Component } from 'react';

class RNCamera extends Component<{}> {
  static Constants = {
    Type: { back: 1 },
    FlashMode: { on: 1 },
  };

  render() {
    return <p>Camera Preview</p>;
  }
}

export { RNCamera };
