import React from 'react';
import TestRenderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import App from '../App'

describe('app', () => {
  it('should render title of the app', () => {
    const testRenderer = TestRenderer.create(<App />);
    const testInstance = testRenderer.root;
    // Extract children strings from rendered Text components
    const textComponentContents = testInstance
      .findAll(i => i.type === 'Text')
      .map(i => i.props.children)
      .reduce((acc, val) => typeof val === 'string'
                              ? `${acc} ${val}`
                              : acc);
    expect(textComponentContents)
      .toEqual(expect.stringContaining('QRCodeReader'));
  });

  it('should render navigation', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('Navigation')).toHaveLength(1);
  })
});
