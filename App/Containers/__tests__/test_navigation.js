import React from 'react';
import { shallow } from 'enzyme';
import Navigation from '../Navigation';

describe('Navigation Container', () => {
  it('should render navigation buttons', () => {
    const wrapper = shallow(<Navigation />);
    expect(wrapper.find('TouchableOpacity')).toHaveLength(2);
  });

  it('should render code scanner feature', () => {
    const wrapper = shallow(<Navigation />);
    wrapper.setState({ activeFeature: 'scan' });
    expect(wrapper.find('CodeScanner')).toHaveLength(1);
  });

  it('should render image load feature', () => {
    const wrapper = shallow(<Navigation />);
    wrapper.setState({ activeFeature: 'load' });
    expect(wrapper.find('ImageLoader')).toHaveLength(1);
  });

  it('should clear selected feature', () => {
    const wrapper = shallow(<Navigation />);
    wrapper.setState({ activeFeature: 'load' });
    wrapper.instance().clearFeatureSelection();
    expect(wrapper.state('activeFeature')).toEqual(null);
  })
});
