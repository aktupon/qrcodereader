import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import CodeScanner from '../Components/CodeScanner';
import ImageLoader from '../Components/ImageLoader';

type State = {
  activeFeature: null | 'scan' | 'load',
};

export default class Navigation extends Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeFeature: null,
    };
  }

  selectFeature = (feature: 'scan' | 'load'): void => {
    this.setState({ activeFeature: feature });
  }

  clearFeatureSelection = (): void => {
    this.setState({ activeFeature: null });
  }

  render() {
    const { activeFeature } = this.state;
    if (activeFeature === 'scan') {
      return (
        <CodeScanner
          successCallback={this.clearFeatureSelection}
        />);
    }
    if (activeFeature === 'load') {
      return (
        <ImageLoader
          successCallback={this.clearFeatureSelection}
        />);
    }
    // if NO feature is selected return navigation
    return (
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={[styles.button, styles.firstButton]}
          onPress={() => this.selectFeature('scan')}
        >
          <Text>Scan</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.selectFeature('load')}
        >
          <Text>Load</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonsContainer: {
    marginTop: 50,
    flexDirection: 'row',
  },
  firstButton: {
    marginRight: 20,
  },
  button: {
    padding: 20,
    borderWidth: 1,
  },
});
