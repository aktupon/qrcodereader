import React from 'react';
import { shallow } from 'enzyme';
import Alert from '../Alert';

describe('Alert Component', () => {
  it('should call Alert api when mounted', () => {
    const mockAlert = jest.fn();
    const wrapper = shallow(
      <Alert
        alert={mockAlert}
      />);
    expect(mockAlert).toHaveBeenCalledTimes(1);
  });
  it('should call dismiss callback on dismiss', () => {
    const mockAlert = jest.fn((title, subtitle, buttons) => {
      const { onPress } = buttons[0];
      onPress();
    });
    const mockSuccessCallback = jest.fn();
    const wrapper = shallow(
      <Alert
        alert={mockAlert}
        dismissCallback={mockSuccessCallback}
      />);
    expect(mockSuccessCallback).toHaveBeenCalledTimes(1);
  });
})
