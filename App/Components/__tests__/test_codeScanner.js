import React from 'react';
import { shallow } from 'enzyme';

import CodeScanner from '../CodeScanner';

describe('CodeScanner Component', () => {
  it('should update state when barcode read event happens', () => {
    const wrapper = shallow(<CodeScanner />);
    expect(wrapper.find('RNCamera')).toHaveLength(1);
    wrapper
      .find('RNCamera')
      .simulate('barCodeRead', { data: 'test' });
    expect(wrapper.state('scannedMessage')).toEqual('test');
  });

  it('should display Alert when scannedMessage exists', () => {
    const wrapper = shallow(<CodeScanner />);
    wrapper.setState({ scannedMessage: 'test' });
    expect(wrapper.find('Alert')).toHaveLength(1);
  });

  it('should call success callback when back button is pressed', () => {
    const mockDismissCallback = jest.fn();
    const wrapper = shallow(
      <CodeScanner
        successCallback={mockDismissCallback}
      />
    );
    wrapper.find('TouchableOpacity').simulate('press');
    expect(mockDismissCallback).toHaveBeenCalledTimes(1);
  });
});
