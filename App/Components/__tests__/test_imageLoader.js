import React from 'react';
import { shallow } from 'enzyme';

import ImageLoader from '../ImageLoader';

describe('ImageLoader Component', () => {
  it('should update scannedMessage with the mocked return value', () => {
    const wrapper = shallow(<ImageLoader />);
    expect(wrapper.state('scannedMessage')).toEqual('dummy-qr-code-message');
  });

  it('should display Alert when scannedMessage exists', () => {
    const wrapper = shallow(<ImageLoader />);
    wrapper.setState({ scannedMessage: 'test' });
    expect(wrapper.find('Alert')).toHaveLength(1);
  });

  it('should display processing indicator when analysis is in progress', () => {
    const wrapper = shallow(<ImageLoader />);
    wrapper.setState({ analysingImage: true });
    expect(wrapper.html()).toEqual(expect.stringContaining('Please wait'));
  });
});
