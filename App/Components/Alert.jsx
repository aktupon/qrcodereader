// @flow
import React from 'react';
import { Alert as AlertAPI } from 'react-native';

const Alert = ({
  message,
  dismissCallback,
  alert = AlertAPI.alert,
}: {
  message: string,
  dismissCallback: () => void,
  alert?: *,
}) => {
  alert(
    `${message}`,
    'scanned by QRCodeReader',
    [
      {
        text: 'OK',
        onPress: dismissCallback,
      },
    ],
    { cancelable: false }
  );
  return null;
};

export default Alert;
