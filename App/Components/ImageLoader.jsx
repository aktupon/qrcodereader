import React, { Component } from 'react';
import { Text } from 'react-native';
import * as ImagePicker from 'react-native-image-picker'
import * as QRCode from '@remobile/react-native-qrcode-local-image';

import Alert from '../Components/Alert';

const options = {
  title: 'Select an image to scan QR Code',
  takePhotoButtonTitle: null,
};

type Props = {
  successCallback: () => void,
}

type State = {
  analysingImage: boolean,
  scannedMessage: ?string,
}

export default class ImageLoader extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      analysingImage: false,
      scannedMessage: null,
    };
  }

  handleDismiss = () => {
    const { successCallback } = this.props;
    this.setState({ scannedMessage: null });
    successCallback();
  }

  handleDecode = (error, result) => {
    const { successCallback } = this.props;
    if (error) return successCallback();
    this.setState({
      scannedMessage: result,
      analysingImage: false,
    })
  }

  componentDidMount() {
    const { successCallback } = this.props;
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel || response.error) {
        successCallback();
      }
      else {
        this.setState({ analysingImage: true });
        QRCode.decode(response.origURL, this.handleDecode);
      }
    });
  }

  render() {
    const { scannedMessage, analysingImage } = this.state;
    if (analysingImage) {
      return <Text>Please wait, Scanning Image...</Text>
    }
    else if (scannedMessage) {
      return (
        <Alert
          dismissCallback={this.handleDismiss}
          message={scannedMessage}
        />);
    }
    return null;
  }
}
