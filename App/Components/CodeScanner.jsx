// @flow
import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import { RNCamera } from 'react-native-camera';

import Alert from './Alert';

const permissionMessages = {
  title: 'Permission to use camera',
  message: 'QRCodeReader needs to use your camera to scan QR codes',
}

type ScanEvent = {
  data: string,
  type: string,
}

type Props = {
  successCallback: () => void,
}

type State = {
  scannedMessage: ?string,
}

export default class CodeScanner extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { scannedMessage: null };
  }

  handleQRCodeScan = (scanEvent: ScanEvent) => {
    this.setState({ scannedMessage: scanEvent.data });
  }

  handleDismiss = () => {
    // Use successCallback to native to menu screen
    const { successCallback } = this.props;
    successCallback();
  }

  render() {
    const { scannedMessage } = this.state;
    return (
      <View style={styles.container}>
        {scannedMessage
         ? (<Alert
              message={scannedMessage}
              dismissCallback={this.handleDismiss}
           />)
         : (<View style={styles.previewWrapper}>
              <RNCamera
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.on}
                permissionDialogTitle={permissionMessages.title}
                permissionDialogMessage={permissionMessages.message}
                onBarCodeRead={this.handleQRCodeScan}
              />
            </View>)
        }
        <TouchableOpacity
          onPress={this.handleDismiss}
          style={styles.button}
        >
          <Text>Back</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  previewWrapper: {
    height: 300,
    width: 200,
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  button: {
    padding: 25,
    textAlign: 'center',
  }
});
